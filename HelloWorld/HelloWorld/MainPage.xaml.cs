﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HelloWorld
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
            InitializeComponent();
            profil.Source = Device.RuntimePlatform == Device.Android ? ImageSource.FromFile("test.jpg") : ImageSource.FromFile("test.jpg");
        }

        void OnButtonClicked(object sender, EventArgs e)
        {
            dataLabel.Text = String.Format("My name is Antoine Mille.\nI am french IT student in CSUSM.\nMy major is Computer Science\n");
        }
    }
}
